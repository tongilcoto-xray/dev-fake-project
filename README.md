# dev-fake-project

This is a fake project just for demostrations

- BAS-3 #2
- BAS-3 #3
- BAS-3 #3 bis 
- BAS-3 #4 Put back to ready for dev when Merge Pipeline crashes
- BAS-3 #5 Repeating #4 ...
- BAS-3 #6 Repeating #4 after unchecking Jira trigger configuration for status transition (here in Gitlab project Settings)
- BAS-3 #7 Adding a new rule for logging build webhook
- BAS-3 #8 Adding logging in the pipeline for getting jira id
- BAS-3 #9 Adding more logging in the pipeline for getting jira id
- BAS-3 #10 Final workaround solution: calling jira api to execute the transition ready for qa -> ready for dev
- BAS-3 #11 NOW Final workaround solution: calling jira api to execute the transition ready for qa -> ready for dev
    - The jira id is get from the branch name --> It is mandatory that the branch name starts with jira id
    - Current Transition Id is 131, it is hardcoded --> Every project will need to update the script
- BAS-3 #12 Adding execution status to execution ticket title